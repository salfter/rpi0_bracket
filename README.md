Raspberry Pi Zero 2020 Bracket
==============================

This is a simple bracket to attach a Raspberry Pi Zero (or similar SBC) to a
piece of 2020 extrusion in your 3D printer.  The 10-mm gap between the board
and the extrusion is to accommodate other parts that might be in the way. 
For instance, in my Hypercube 300, a Z-axis rod mount is right about where I
want to put a Raspberry Pi Zero 2 W to keep it close to the main control
board.

For whatever it's worth, this is also my first design brought to fruition
with FreeCAD.  I've tended to use OpenSCAD for similar things in the past,
but just figured I'd try something different.
